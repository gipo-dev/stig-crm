/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';
import LayoutFree from '@/layout/freeIndex';

const adminRoutes = {
  path: '',
  component: Layout,
  redirect: '/users',
  name: 'Administrator',
  alwaysShow: true,
  meta: {
    title: 'Администрирование',
    icon: 'admin',
    permissions: ['view menu administrator'],
  },
  children: [
    /** User managements */
    {
      path: 'workers',
      component: LayoutFree,
      redirect: '/workers/list',
      name: 'Users',
      meta: {title: 'Сотрудники', icon: 'worker', permissions: ['manage user'], noCache: true},
      children: [
        {
          path: 'list',
          component: () => import('@/views/users/list'),
          name: 'UserList',
          meta: {title: 'Текущие заказы'},
        },
        {
          path: ':id(\\d+)/edit',
          component: () => import('@/views/users/edit'),
          name: 'UserEdit',
          meta: {title: 'Управление аккаунтом', noCache: true, permissions: ['manage user']},
        },
        {
          path: 'create/:role',
          component: () => import('@/views/users/edit'),
          name: 'UserCreate',
          meta: {title: 'Новый аккаунт', noCache: true, permissions: ['manage user']},
        },
      ],
    },
    /** Role and permission */
    // {
    //   path: 'roles',
    //   component: () => import('@/views/role-permission/List'),
    //   name: 'RoleList',
    //   meta: {title: 'rolePermission', icon: 'role', permissions: ['manage permission']},
    //   hidden: true,
    // },
  ],
};

export default adminRoutes;
