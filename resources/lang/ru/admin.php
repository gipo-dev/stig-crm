<?php

return [

    'roles' => [
        'admin' => 'Директор',
        'manager' => 'Менеджер',
        'warehouse' => 'Склад',
        'production' => 'Производство',
    ],

    'order' => [
        'payment' => [
            'card' => 'Предоплата',
            'cash' => 'Наличными',
        ],
        'statuses' => [
            'keys' => [
                \App\Laravue\Models\OrderEvent::EVENT_WAIT_FOR_ACCEPT => 'Оставлена заявка',
                \App\Laravue\Models\OrderEvent::EVENT_WAIT_FOR_CONFIRM => 'Подтверждение заявки менеджером',
                \App\Laravue\Models\OrderEvent::EVENT_WAITING_FOR_PAYMENT => 'Получение оплаты',
                \App\Laravue\Models\OrderEvent::EVENT_ASSEMBLY => 'Сборка заказа на складе',
                \App\Laravue\Models\OrderEvent::EVENT_DELIVERY_TO_TRANSPORT_COMPANY => 'Доставка в транспортную компанию',
                \App\Laravue\Models\OrderEvent::EVENT_DELIVERY => 'Доставка',
                \App\Laravue\Models\OrderEvent::EVENT_FINISH => 'Заказ получен клиентом',
                \App\Laravue\Models\OrderEvent::EVENT_WAIT_FOR_CLIENT => 'Вручение заказа клиенту',
            ],
            'titles' => [
                \App\Laravue\Models\OrderEvent::EVENT_WAIT_FOR_ACCEPT => 'Оставлена заявка',
                \App\Laravue\Models\OrderEvent::EVENT_WAIT_FOR_CONFIRM => 'Подтверждение заявки менеджером',
                \App\Laravue\Models\OrderEvent::EVENT_WAITING_FOR_PAYMENT => 'Подтверждение оплаты',
                \App\Laravue\Models\OrderEvent::EVENT_ASSEMBLY => 'Сборка заказа на складе',
                \App\Laravue\Models\OrderEvent::EVENT_DELIVERY_TO_TRANSPORT_COMPANY => 'Доставка в транспортную компанию',
                \App\Laravue\Models\OrderEvent::EVENT_DELIVERY => 'Доставка',
                \App\Laravue\Models\OrderEvent::EVENT_FINISH => 'Получение заказа клиентом',
                \App\Laravue\Models\OrderEvent::EVENT_WAIT_FOR_CLIENT => 'Вручение заказа клиенту',
                '' => 'Заказ получен клиентом',
            ],
            'colors' => [
                \App\Laravue\Models\OrderEvent::EVENT_WAIT_FOR_ACCEPT => \App\Laravue\Models\OrderEvent::COLOR_SUCCESS,
                \App\Laravue\Models\OrderEvent::EVENT_WAIT_FOR_CONFIRM => \App\Laravue\Models\OrderEvent::COLOR_WARNING,
                \App\Laravue\Models\OrderEvent::EVENT_WAITING_FOR_PAYMENT => \App\Laravue\Models\OrderEvent::COLOR_DANGER,
                \App\Laravue\Models\OrderEvent::EVENT_ASSEMBLY => \App\Laravue\Models\OrderEvent::COLOR_PRIMARY,
                \App\Laravue\Models\OrderEvent::EVENT_DELIVERY_TO_TRANSPORT_COMPANY => \App\Laravue\Models\OrderEvent::COLOR_PRIMARY,
                \App\Laravue\Models\OrderEvent::EVENT_DELIVERY => \App\Laravue\Models\OrderEvent::COLOR_PRIMARY,
                \App\Laravue\Models\OrderEvent::EVENT_FINISH => \App\Laravue\Models\OrderEvent::COLOR_WARNING,
                \App\Laravue\Models\OrderEvent::EVENT_WAIT_FOR_CLIENT => \App\Laravue\Models\OrderEvent::COLOR_WARNING,
                '' => \App\Laravue\Models\OrderEvent::COLOR_SUCCESS,
            ],
            'types' => [
                'card' => [
                    \App\Laravue\Models\OrderEvent::EVENT_WAIT_FOR_ACCEPT,
                    \App\Laravue\Models\OrderEvent::EVENT_WAIT_FOR_CONFIRM,
                    \App\Laravue\Models\OrderEvent::EVENT_WAITING_FOR_PAYMENT,
                    \App\Laravue\Models\OrderEvent::EVENT_ASSEMBLY,
                    \App\Laravue\Models\OrderEvent::EVENT_DELIVERY_TO_TRANSPORT_COMPANY,
                    \App\Laravue\Models\OrderEvent::EVENT_DELIVERY,
                    \App\Laravue\Models\OrderEvent::EVENT_FINISH,
                ],
                'cash' => [
                    \App\Laravue\Models\OrderEvent::EVENT_WAIT_FOR_ACCEPT,
                    \App\Laravue\Models\OrderEvent::EVENT_WAIT_FOR_CONFIRM,
                    \App\Laravue\Models\OrderEvent::EVENT_ASSEMBLY,
                    \App\Laravue\Models\OrderEvent::EVENT_WAIT_FOR_CLIENT,
                    \App\Laravue\Models\OrderEvent::EVENT_FINISH,
                ],
            ],
            'buttons' => [
                'card' => [
                    \App\Laravue\Models\OrderEvent::EVENT_WAIT_FOR_ACCEPT => 'Подтвердить заявку',
                    \App\Laravue\Models\OrderEvent::EVENT_WAIT_FOR_CONFIRM => 'Подтвердить оплату',
                    \App\Laravue\Models\OrderEvent::EVENT_WAITING_FOR_PAYMENT => 'Заказ собран',
                    \App\Laravue\Models\OrderEvent::EVENT_ASSEMBLY => 'Заказ доставляется в тк',
                    \App\Laravue\Models\OrderEvent::EVENT_DELIVERY_TO_TRANSPORT_COMPANY => 'Заказ отправлен через тк',
                    \App\Laravue\Models\OrderEvent::EVENT_DELIVERY => 'Заказ получен клиентом',
                    \App\Laravue\Models\OrderEvent::EVENT_WAIT_FOR_CLIENT => 'Клиент забрал и оплатил заказ',
                ],
                'cash' => [
                    \App\Laravue\Models\OrderEvent::EVENT_WAIT_FOR_ACCEPT => 'Подтвердить заявку',
                    \App\Laravue\Models\OrderEvent::EVENT_WAIT_FOR_CONFIRM => 'Заказ собран',
                    \App\Laravue\Models\OrderEvent::EVENT_ASSEMBLY => 'Заказ готов к вручению',
                    \App\Laravue\Models\OrderEvent::EVENT_WAIT_FOR_CLIENT => 'Клиент оплатил заказ и получил его',
                    \App\Laravue\Models\OrderEvent::EVENT_FINISH => 'Клиент оплатил заказ и получил его',
                ],
            ],
        ],
    ],

];
