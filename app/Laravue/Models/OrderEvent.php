<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderEvent extends Model
{
    public const EVENT_WAIT_FOR_ACCEPT = 10, //Оставлена заявка
        EVENT_WAIT_FOR_CONFIRM = 20, //Подтверждение заявки менеджером
        EVENT_WAITING_FOR_PAYMENT = 30, //Получение оплаты
        EVENT_ASSEMBLY = 40, //Сборка заказа на складе
        EVENT_DELIVERY_TO_TRANSPORT_COMPANY = 50, //Доставка в транспортную компанию
        EVENT_DELIVERY = 60, //Доставка
        EVENT_FINISH = 200, //Заказ получен клиентом
        EVENT_WAIT_FOR_CLIENT = 70; //Вручение заказа клиенту

    public const COLOR_PRIMARY = 'primary',
        COLOR_WARNING = 'warning',
        COLOR_SUCCESS = 'success',
        COLOR_DANGER = 'danger';

    /**
     * @var array
     */
    protected $guarded = [];
}
