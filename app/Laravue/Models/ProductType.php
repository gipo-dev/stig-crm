<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * Class ProductType
 * @package App\Models
 *
 * @property string $full_name
 */
class ProductType extends Model
{
    use AsSource;
    use Filterable;

    /**
     * @var string[]
     */
    protected $with = [
        'parent',
    ];

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->parent->category;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function prices()
    {
        return $this->hasMany(ProductPrice::class, 'product_id');
    }

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        if (!$this->parent)
            return $this->name;
        return $this->parent->name . ' ' . $this->name;
    }
}
