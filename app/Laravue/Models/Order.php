<?php

namespace App\Laravue\Models;

use App\Laravue\Models\OrderDelivery;
use App\Laravue\Models\OrderEvent;
use App\Laravue\Models\User;
use App\Services\OrderEventService;
use App\Services\OrderProductService;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var string[]
     */
    protected $with = [
        'client',
        'events',
        'delivery',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'cart' => 'array',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function delivery()
    {
        return $this->belongsTo(OrderDelivery::class, 'delivery_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function events()
    {
        return $this->hasMany(OrderEvent::class)->latest();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getProducts()
    {
        return collect($this->cart['cart'])->map(function ($item) {
            return collect($item);
        });
    }

    /**
     * @return \App\Services\OrderEventService
     */
    public function eventService()
    {
        return new OrderEventService($this);
    }

    /**
     * @return float|int
     */
    public function getTotalSumAttribute()
    {
        $total = 0;
        foreach ($this->cart['cart'] ?? [] as $item) {
            $total += (new OrderProductService($item))->getPrice($item['quantity'] ?? 1)
                * ($item['quantity'] ?? 1);
        }
        return $total;
    }
}
