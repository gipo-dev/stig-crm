<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDelivery extends Model
{
    /**
     * @var string
     */
    protected $table = 'saved_deliveries';

    /**
     * @var array
     */
    protected $guarded = [];
}
