<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    /**
     * @var array
     */
    protected $guarded = [];
}
