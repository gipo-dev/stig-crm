<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UploadController extends Controller
{
    const UPLOAD_PATH = '/images';

    /**
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function upload(Request $request)
    {
        if ($request->hasFile('file') and $request->file('file')->isValid()) {
            // проверка данных
            $allow = array('image/jpeg', 'image/png', 'image/gif');

            $mine = $request->file('file')->getMimeType();
            if (!in_array($mine, $allow)) {
                return response()->json(['status' => 0, 'msg' => 'Неправильный тип файла, могут быть загружены только изображения']);
            }

            // исходное изображение, загрузить на локальный
            $path = $request->file->store('public' . self::UPLOAD_PATH);

            // Абсолютный путь
            $file_path = storage_path('app/') . $path;
        }
        // Вставить таблицу изображений
        return ['image' => basename($file_path), 'image_url' => '/storage' . self::UPLOAD_PATH . '/' . basename($file_path)];
    }
}
