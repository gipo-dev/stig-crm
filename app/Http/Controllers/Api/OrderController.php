<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\OrderProductResource;
use App\Http\Resources\OrderResource;
use App\Laravue\Models\Order;
use App\Laravue\Models\OrderEvent;
use App\Services\OrderEventService;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $pagination = Order::orderBy($request->get('sort_prop', 'id'), $request->get('sort_order', 'desc'))
            ->paginate($request->get('limit'));
        return [
            'items' => OrderResource::collection($pagination),
            'total' => $pagination->total(),
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Laravue\Models\Order $order
     * @return array
     */
    public function show(Request $request, Order $order)
    {


        return [
            'order' => OrderResource::make($order),
            'products' => OrderProductResource::collection($order->getProducts()),
            'totalSum' => $order->total_sum,
            'statuses' => $order->eventService()->statusList(),
            'nextStatus' => [
                'id' => $order->eventService()->nextStatus(),
                'text' => $order->eventService()->getButtonText(),
            ],
            'history' => OrderResource::collection($order->client->orders()
                ->whereHas('events', function ($q) {
                    $q->whereIn('event_id', [OrderEvent::EVENT_FINISH,]);
                })->get()),
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Laravue\Models\Order $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Laravue\Models\Order $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Laravue\Models\Order $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Laravue\Models\Order $order
     * @return void
     */
    public function setStatus(Request $request, Order $order)
    {
        $order->events()->create([
            'event_id' => $request->get('statusId'),
        ]);
    }
}
