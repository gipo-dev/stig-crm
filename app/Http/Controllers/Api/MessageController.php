<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Chat\ChatResource;
use App\Http\Resources\Chat\MessageResource;
use App\Laravue\Models\Message;
use App\Laravue\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param $chatId
     * @return \App\Http\Resources\Chat\ChatResource
     */
    public function show(Request $request, $chatId)
    {
        $order = Order::find($chatId);
        $messages = Message::where('chat_id', $chatId);

        if ($request->type)
            $messages->where('chat_type', $request->type);
        else
            $messages->whereNull('chat_type');

        return (new ChatResource($messages->get(), $order->user_id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Laravue\Models\Message $message
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Laravue\Models\Message $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Laravue\Models\Message $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        //
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $chatId
     * @return array
     */
    public function send(Request $request, $chatId) {
        $message = Message::create([
            'user_id' => Auth::id(),
            'chat_id' => $chatId,
            'type' => 'message',
            'message' => $request->message,
        ]);
        return (new MessageResource($message))->toArray($request);
    }
}
