<?php

namespace App\Http\Resources;

use App\Services\OrderProductService;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $price = (new OrderProductService($this))->getPrice($this['quantity'] ?? 0);
        return [
            'id' => $this['product_id'] ?? $this['id'] ?? 0,
            'name' => $this['name'] ?? '',
            'img' => $this['photo'] ?? '',
            'count' => $this['quantity'] ?? '',
            'price' => $price ?? '',
            'price_total' => $price * ($this['quantity'] ?? 0),
        ];
    }
}
