<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    const DELIVERY_TYPES = [
        'Адрес' => 'address',
        'СДЭК' => 'cdek',
        'Самовывоз' => 'pickup',
    ];

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'payment' => __('admin.order.payment.' . $this->payment_type),
            'payment_type' => $this->payment_type,
            'client' => (new ClientResource($this->client))->toArray($request),
            'status' => __('admin.order.statuses.titles.' . $this->eventService()->nextStatus()),
            'delivery' => [
                'type' => self::DELIVERY_TYPES[$this->delivery->type ?? 'Адрес'],
                'address' => $this->delivery->adress,
            ],
            'created_at' => [
                'default' => $this->created_at,
                'date' => $this->created_at->format('d.m.Y'),
                'time' => $this->created_at->format('H:i'),
            ],
        ];
    }
}
