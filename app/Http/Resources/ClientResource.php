<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class ClientResource extends JsonResource
{
    public static $currentIndex = -1;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'surname' => $this->surname,
            'father_name' => $this->father_name,
            'short_name' => $this->getShortName(),
            'username' => $this->username,
            'email' => $this->email,
            'phone' => $this->phone,
            'avatar' => $this->getAvatar(),
            'avatar_url' => $this->avatar,
            'birthday' => $this->birthday ? $this->birthday->format('d.m.Y') : '',
            'registered' => $this->created_at ? $this->created_at->format('d.m.Y') : '',
            'role' => __('admin.roles.' . $this->roles->first()->name),
            'password' => $this->getPassword(),
        ];
    }

    /**
     * @return string
     */
    private function getShortName()
    {
        if ($this->surname) {
            $name = $this->surname . ' ';
            $name .= mb_substr($this->name, 0, 1) . '. ';
        } else
            $name = $this->name . ' ';

        if ($this->father_name)
            $name .= mb_substr($this->father_name, 0, 1) . '. ';

        return $name;
    }

    private const AVATAR_COLORS = [
        '#F85E5E',
        '#F98858',
        '#F9B858',
        '#7AA740',
        '#72A37A',
        '#106C51',
        '#10566C',
        '#5EB6D1',
        '#1A6BB5',
        '#0E4170',
        '#2A19F5',
        '#833EC9',
        '#70115B',
        '#B60C20',
        '#F4223B',
    ];

    /**
     * @return string
     */
    public function getAvatar()
    {
        if ($this->avatar) {
            return "<img src='$this->avatar'>";
        }

        $name = '';
        if ($this->surname)
            $name = mb_substr($this->surname, 0, 1);
        $name .= mb_substr($this->name, 0, 1);

        return '<span style="background: ' . self::getAvatarColor() . '">' . mb_strtoupper($name) . '</span>';
    }

    /**
     * @return string
     */
    private static function getAvatarColor()
    {
        self::$currentIndex++;
        if (self::$currentIndex >= count(self::AVATAR_COLORS))
            self::$currentIndex = 0;
        return self::AVATAR_COLORS[self::$currentIndex];
    }

    /**
     * @return mixed|string
     */
    private function getPassword()
    {
        if (Auth::user()->isAdmin() && Auth::user()->hasPermission(\App\Laravue\Acl::PERMISSION_USER_MANAGE)) {
            return $this->open_password;
        }
        return '';
    }
}
