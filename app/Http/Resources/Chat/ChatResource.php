<?php

namespace App\Http\Resources\Chat;

use Illuminate\Http\Resources\Json\JsonResource;

class ChatResource extends JsonResource
{
    public function __construct($resource, $contactId)
    {
        parent::__construct($resource);
        MessageResource::setContact($contactId);
    }

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'messageList' => MessageResource::collection($this->resource),
            'participants' => [
                [
                    'id' => 'me',
                    'name' => 'Me',
                    'imageUrl' => '',
                ],
                [
                    'id' => 'user',
                    'name' => 'User',
                    'imageUrl' => '',
                ],
            ],
        ];
    }
}
