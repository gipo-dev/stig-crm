<?php

namespace App\Http\Resources\Chat;

use Illuminate\Http\Resources\Json\JsonResource;

class MessageResource extends JsonResource
{
    private const TYPE_ALIAS = [
        'message' => 'text',
        'attachment' => 'file',
    ];

    private static $contactId;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => self::TYPE_ALIAS[$this['type']],
            'author' => $this['user_id'] == self::$contactId ? 'user' : 'me',
            'data' => [
                'id' => $this['id'],
                'text' => $this['message'],
                'time' => $this['created_at']->format('H:i'),
                'date' => null,
                'file' => [
                    'name' => '12',
                    'url' => $this['attachment'],
                ],
            ],
        ];
    }

    /**
     * @param $contactId
     * @return void
     */
    public static function setContact($contactId)
    {
        static::$contactId = $contactId;
    }
}
