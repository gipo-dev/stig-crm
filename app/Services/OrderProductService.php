<?php

namespace App\Services;

class OrderProductService
{
    private $product;

    public function __construct($product)
    {
        $this->product = $product;
    }

    /**
     * @return int|mixed
     */
    public function getPrice($count)
    {
        $currentPrice = 0;
        if (!isset($this->product['prices']))
            return $this->product['price'];
        foreach ($this->product['prices'] as $price) {
            if ($price['to'] == null || $count < $price['to'])
                $currentPrice = $price['price'];
        }
        return $currentPrice;
    }
}
