<?php

namespace App\Services;

use App\Laravue\Models\Order;
use App\Laravue\Models\OrderEvent;
use Illuminate\Support\Facades\Lang;

class OrderEventService
{
    /**
     * @var \App\Laravue\Models\Order
     */
    private $order;

    /**
     * @var int
     */
    private $_status;

    /**
     * @var int
     */
    private $_nextStatus;

    /**
     * @param \App\Laravue\Models\Order $order
     */
    public function __construct(Order $order)
    {
        if ($order->events->count() < 1) {
            $order->events()->create([
                'event_id' => OrderEvent::EVENT_WAIT_FOR_ACCEPT,
                'created_at' => $order->created_at,
            ]);
            $order = $order->refresh();
        }
        $this->order = $order;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        if (!$this->_status)
            $this->_status = $this->order->events->sortByDesc('id')->first()->event_id;
        return $this->_status;
    }

    /**
     * @return array|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Translation\Translator|string|null
     */
    public function statusList()
    {
        $statuses = [];
        $events = $this->order->events->keyBy('event_id');
        foreach (__('admin.order.statuses.types.' . $this->order->payment_type) as $event_id)
            $statuses[$event_id] = [
                'name' => __('admin.order.statuses.titles.' . $event_id),
                'date' => isset($events[$event_id]) ? $events[$event_id]->created_at->format('d.m.Y H:i') : null,
                'current' => $this->nextStatus() == $event_id,
            ];
        return $statuses;
    }

    /**
     * @return int|null
     */
    public function nextStatus()
    {
        $statuses = __('admin.order.statuses.types.' . $this->order->payment_type);
        if (!$this->_nextStatus) {
            $this->_nextStatus = null;
            if (($statusIndex = array_search($this->getStatus(), $statuses)) !== false) {
                $this->_nextStatus = $statuses[$statusIndex + 1] ?? null;
            }
        }
        return $this->_nextStatus;
    }


    public function getButtonText()
    {
        $paymentType = $this->order->payment_type;
        $statusId = $this->order->eventService()->getStatus();
        if (Lang::has('admin.order.statuses.buttons.' . $paymentType . '.' . $statusId))
            return __('admin.order.statuses.buttons.' . $paymentType . '.' . $statusId);
        return null;
    }
}
